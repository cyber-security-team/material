In today's lesson we started with Ethan's vulnerability presentation. The
vulnerability he highlighted was how one could inject JavaScript code into a
client with malicious content. In order to prevent malicious content from being
injected, he added a fix that would sanitize the input from the user.

After his presentation, we moved on to some more low level / reverse
engineering material. More Specifically, we dedicated most of today to learning
assembly (and also some Radare2).

We started off with a quick trick, which is to compile an existing C program
into assembly using GCC

```
gcc -S main.c
```

We then edited the `main.s` file which would look something like this

```
	.file	"main.c"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
```

In order to get only the code we need, we edited down the file to

```
	.globl	main
	.type	main, @function
main:
	pushq	%rbp
	movq	%rsp, %rbp
	movl	$0, %eax
	popq	%rbp
	ret

```

This was already much cleaner, and we also had a base to start working with
(and this also gave me some room to first explain the basic instructions before
explaining labels).

We started by covering a few instructions: `mov`, `add`, `sub`, and `jmp`. Once
they were comfortable playing with those instructions, I also introduced `jle`
and `jge`, and they started working on a program that prints out 9 *s in a row.

At this point, 2 students had to leave for other classes, so the 2 students 
that remained got a sneak peek at Radare2. However, I'm planning to cover that
again next class since that way everybody would be on the same page.

Hopefully next class everybody would be able to stay until the end, so that
nobody would fall behind.

If you have any questions, I can be reached at _yuval@applied-computing.org_

