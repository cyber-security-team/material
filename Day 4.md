In today's lesson, we skipped the presentation since we were running somewhat short on time due 
to some scheduling issues. The main focus of today was to get set up for next time, since repl.it
was deleting our binaries for some reason. Currently, we were using virtual machines so that they
could have full control over their environment.

We first started by installing Virtualbox, Oracle's VM manager.

![Virtualbox UI](Day 4 1.png)

We then downloaded the latest Debian ISO, and using Virtualbox, everyone installed Debian on their
own virtual machines.

![Debian GRUB](Day 4 2.png)

After getting everything set up, I just had them log in to GitLab, and try out running a binary I
compiled on my Ubuntu machine.

The homework for today was to install Radare2 for next time, so that we can start writing some
crackmes.

If you have any questions, I can be reached at _yuval@applied-computing.org_
