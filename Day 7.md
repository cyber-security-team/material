In today's lesson we started off by looking over the login program they created last time. Some of them did finish, though since a lot of them were busy with AP testing, we took about an hour of pair programming to finish up writing the basic program without implementing hashes yet.

```python
#!/usr/bin/python3

import json

db = json.loads(open("db.json", "r").read())

def login():
    u = input("Username: ")
    p = input("Password: ")
    try:
        if db[u] == p:
            print("Correct!")
        else:
            print("Incorrect Password")
			print(p
    except KeyError:
        if input("User does not exist. Make a new one? [y/N] ").lower() == 'y':
            register()
        else:
            login()

def register():
    u = input("Username: ")
    p = input("Password: ")
    pc = input("Confirm Password: ")

    while pc != p:
        print("Passwords don't match, please try again...")
        p = input("Password: ")
        pc = input("Confirm Password: ")

    try:
        if input("User " + db[u] + " exists. Login? [y/N] ").lower() == 'y':
            login()
        else:
            register()
    except KeyError:
        db[u] = p

if __name__ == "__main__":
    if input("Are you an existing user? [y/N] ").lower() == 'y':
        login()
    else:
        register()

    open("db.json", "w").write(json.dumps(db))
```

Once they finished with that, we started looking at using the `bcrypt` library to hash the passwords for our users. Initially, we all thought that the best idea would've been to hardcode the salt somewhere, though Ethan pointed out that the salt is actually encoded inside of the hash itself, so there was no need to even store the salt in the first place. For the rest of the class we mainly just talked about bcrypt.

The homework was mainly to just perfect the program and finish it if they haven't already. Next time we'll probably go over symmetric encryption and AES.

If you have any questions, I can be reached at _yuval@applied-computing.org_
