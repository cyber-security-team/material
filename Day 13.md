### Class Summary:

In today's lesson we started out by going over what progress we made from the previous class, which consisted of either completing more Over The Wire challenges (mainly natas and leviathan) and progress on Conway's Game of Life project. We then allowed the students to continue working their projects, helping review topics and deal with bugs (SDL2 errors were particularly pesky).

After giving the students some time, we decided to converge as a group to discuss the Luhn algorithm and its uses in validating credit card numbers. We gave a few examples of how the algorithm worked and showed an example of both a valid and an invalid card number before discussing a few approaches to implement this algorithm. We then had the students spend the rest of the class time writing their program in Python to implement the Luhn algorithm before we concluded class.

### Homework: 

Continue working on personal projects and Over The Wire challenges.

Make end-of-course wrap up presentations for next class. Try to include at least 5 slides. Screenshots and demos of things you've made would be great to include as well!

Slide Ideas: 
- What you have learned
- What you have created
- What you want to learn
- Most interesting topic
- Most applicable topic
- etc.
