In today's lesson we started off by getting radare2 set up for everybody on
their virutal machines. It turns out a lot of them had trouble getting the PATH
environment variable working properly, so we went ahead and fixed that by
changing the PATH in the `.bashrc` file.

Once we got that working, we started by creating a basic program to check if the
user entered the correct key when running the program from the command line.

```c
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
	if (argc > 1 && !strcmp(argv[1], "S3CR3T") ){
		printf("Correct!\n");
	}
	return 0;
}
```

However, when opening this up with radare2, we can clearly see the key. The
students took the rest of the class to make their programs more difficult to
crack by encrypting their passwords using ciphers. It's still possible to crack
those ciphers, since it can still be traced.

There wasn't any special homework this time except for finishing up the crackme
if you didn't have time.

If you have any questions, I can be reached at _yuval@applied-computing.org_
