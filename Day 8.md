In today's meeting, it seemed as if they still didn't finish writing the password manager homework. Therefore, we spent the first section of the lesson on pair programming to finish the password manager.

```python
#!/usr/bin/python3

import json

if __name__ == "__main__":
	db = json.loads(open("db.json", "r").read())
	inp = ""

	while not "exit" in inp.lower():
		if "get" in inp.lower():
			try:
				print(db[input("username: ")])
			except KeyError:
				print("ERROR: username not found")
		elif "add" in inp.lower():
			username = input("username: ")
			try:
				print("ERROR: username %s already exists" % db[username])
			except KeyError:
				password = input("password: ")
				db[username] = password
		inp = input("would you like to GET or ADD a password? ")
	open("db.json", "w").write(json.dumps(db))
```

After we finished getting it working, we covered what symmetric and asymmetric encryption is, and some of the use cases for symmetric encryption. After a little bit of talking, we decided that this project could be split up among the 4 of them.

The role distribution was as follows:
![Roles](Day 8 1.png)

The homework was just to finish the app for next time, so that we could move on to making the messenger app (hopefully).

If you have any questions, I can be reached at _yuval@applied-computing.org_
